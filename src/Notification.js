import React from "react";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import "./App.css";

function Notiification({ seat, limit, trigger, socket }) {
   if (!trigger) return null;

   const handleCancel = () => {
      socket.emit("clearTimer", seat);
   };

   return (
      <div className="notifyContainer">
         <Typography variant="h5" className="notifySeat">
            {seat}
         </Typography>
         <IconButton
            aria-label="delete"
            className="notifyCancel"
            size="large"
            onClick={handleCancel}
         >
            <HighlightOffIcon fontSize="large" />
         </IconButton>
         <Typography lang="zh" variant="h6" className="notifyBody">
            {Math.floor(limit / 60)} 分鐘計時已完成。
         </Typography>
      </div>
   );
}
export default Notiification;
