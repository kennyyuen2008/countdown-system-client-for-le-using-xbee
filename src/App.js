import React, { useState, useEffect } from "react";
import "./App.css";
import webSocket from "socket.io-client";
import Timer from "./Timer";
import Notification from "./Notification";
import { Typography } from "@material-ui/core";

const uri = "http://localhost:3000";
//const options = { transports: ["websocket"] };

function App() {
   const [ws, setWs] = useState(null);
   const [rows, setRows] = useState([]);

   useEffect(() => {
      setWs(webSocket(uri));
   }, []);

   useEffect(() => {
      if (ws) {
         // connect successful
         console.log("connect successful");
         ws.on("showrows", data => {
            setRows(data);
         });
      }
   }, [ws]);

   return (
      <div className="app">
         <div>
            <Typography className="title" lang="zh" variant="h5">
               進行中
            </Typography>
            <div className="timersPanel">
               {rows.map(row => (
                  <Timer
                     key={row.seat}
                     seat={row.seat}
                     limit={row.totaltimelimit}
                     start={row.starttime}
                     remain={row.remainingtime}
                     pause={row.pause}
                     socket={ws}
                  />
               ))}
            </div>
         </div>
         <div>
            <Typography className="title" lang="zh" variant="h5">
               已完成
            </Typography>
            <div className="notifyPanel">
               {rows.map(row => (
                  <Notification
                     key={row.seat}
                     seat={row.seat}
                     limit={row.totaltimelimit}
                     trigger={row.notify}
                     socket={ws}
                  />
               ))}
            </div>
         </div>
      </div>
   );
}

export default App;
