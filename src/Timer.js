import React, { useState, useEffect } from "react";
import "./App.css";
import Typography from "@material-ui/core/Typography";
import AlarmIcon from "@material-ui/icons/Alarm";
import AlarmOffIcon from "@material-ui/icons/AlarmOff";

function Timer({ seat, limit, start, remain, pause, socket }) {
   const [min, setMin] = useState();
   const [sec, setSec] = useState();
   const [display, setDisplay] = useState(true);

   useEffect(() => {
      let interval;

      if (!pause) {
         interval = setInterval(() => {
            let current = Math.floor(Date.now() / 1000);
            let distance = start + remain - current;
            let mins = Math.floor(distance / 60);
            setMin(mins);
            setSec(distance - mins * 60);
            if (distance <= 0) {
               setDisplay(false);
               clearInterval(interval);
               if (socket) socket.emit("notifyTimer", seat);
            }
         }, 1000);
      } else {
         let mins = Math.floor(remain / 60);
         setMin(mins);
         setSec(remain - mins * 60);
         clearInterval(interval);
      }
      return () => clearInterval(interval);
   }, [pause, seat, socket, start, remain]);

   if (!display) {
      return null;
   }

   return (
      <div className="timerContainer" lang="zh">
         {pause ? (
            <Typography className="timerIcon paused">
               <AlarmOffIcon />
            </Typography>
         ) : (
            <Typography className="timerIcon counting">
               <AlarmIcon />
            </Typography>
         )}
         <Typography className="timerSeat" variant="h5">
            {seat}
         </Typography>
         <Typography className="timerLimit" variant="h6">
            {Math.floor(limit / 60)} 分鐘
         </Typography>
         <Typography variant="h6" component="h2" className="timerRemain">
            {min} 分 {sec} 秒
         </Typography>
      </div>
   );
}

export default Timer;
